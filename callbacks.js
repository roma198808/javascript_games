// ADDNUMBERS

// var readline = require('readline');
// var reader = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout
// });
//
// function addNumbers(sum, numsLeft, completionCallback) {
//   if (numsLeft > 0) {
//     reader.question("Enter your number", function (userInput) {
//       var userInteger = parseInt(userInput);
//       sum += userInteger;
//       console.log("Current sum: " + sum);
//       addNumbers(sum, numsLeft - 1, completionCallback);
//     })
//   } else {
//     completionCallback(sum);
//   }
// }
//
// addNumbers(0, 3, function (sum) {
//   console.log("Total Sum: " + sum);
//   reader.close();
// });






Function.prototype.myBind = function(obj) {
  var fun = this;
   return function() { fun.apply(obj, []) };
};

// CLOCK

function Clock(currentTime){
  this.seconds = currentTime.getSeconds();
  this.minutes = currentTime.getMinutes();
  this.hours = currentTime.getHours();
};

Clock.prototype.tick = function(){
  this.seconds += 5;
  if(this.seconds >= 60){
    this.seconds -= 60;
    this.minutes += 1;
  }
  if(this.minutes >= 60){
    this.minutes -= 60;
    this.hours += 1;
  }
  if(this.hours >= 24){
    this.hours -= 24;
  }
  this.display();
};

Clock.prototype.display = function(){
  console.log(this.hours + ":" + this.minutes + ":" + this.seconds);
};

var clock = new Clock(new Date());

// setInterval(clock.tick.myBind(clock), 5000);

