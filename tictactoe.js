(function (root) {
  var Tic = root.Tic = (root.Tic || {});

  var reader;
  var readline = require('readline');


  var Game = Tic.Game = function() {
    this.board = [[NaN,NaN,NaN],[NaN,NaN,NaN],[NaN,NaN,NaN]];
  };

  Game.prototype.run = function(gameOver, moveType) {
    this.display();

    if (gameOver) {
      console.log(moveType + " has won!");
      return;
    } else {
      moveType = (moveType == "X") ? "O" : "X";
      this.getMove(moveType);
    }
  };

  Game.prototype.display = function() {
    for (var i = 0; i < this.board.length; i++) {
      console.log(this.board[i]);
    }
  };

  Game.prototype.getMove = function(moveType) {
    var parsedAnswer1, parsedAnswer2;
    var game = this;
    reader = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    console.log(moveType + " maka a move:");
    reader.question("row: ", function(answer1) {
      reader.question("column: ", function(answer2) {
        parsedAnswer1 = parseInt(answer1);
        parsedAnswer2 = parseInt(answer2);

        game.makeMove(parsedAnswer1, parsedAnswer2, moveType);
      })
    });
  };

  Game.prototype.makeMove = function(row, col, moveType) {
    reader.close();
    this.board[row][col] = moveType;
    this.checkGameOver(moveType);
  };

  Game.prototype.checkGameOver = function(moveType) {
    var win = false;
    for(i = 0; i < this.board.length; i++){
      if(this.board[0][i] == this.board[1][i] &&
         this.board[0][i] == this.board[2][i]){
           win = true;
      } else if(this.board[i][0] == this.board[i][1] &&
                this.board[i][0] == this.board[i][2]){
         win = true;
      } else if(this.board[0][0] == this.board[1][1] &&
                this.board[0][0] == this.board[2][2]){
        win = true;
      } else if(this.board[2][0] == this.board[1][1] &&
                this.board[2][0] == this.board[0][2]){
        win = true;
      }
    }

    this.run(win, moveType);
  };

})(this);

g = new this.Tic.Game;
g.run(false, "X");