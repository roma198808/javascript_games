Function.prototype.myBind = function(obj) {
  var  fun = this;
  return function() { fun.apply(obj, []) };
};

function Dog(name){
  this.name = name;
  this.bark = function(){
    console.log(this.name + " barks!")
  }
};

d = new Dog("spike");

var something = function(){
  d.bark.myBind(d);
};


var times = function(n, callback){
  for (var i = 0; i < n; i++) {
    callback();
  }
};
