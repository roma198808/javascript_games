(function (root) {
  var Hanoi = root.Hanoi = (root.Hanoi || {});

  var Game = Hanoi.Game = function Game () {
    this.stacks = [[3,2,1], [], []];
  };

  var reader;

  Game.prototype.display = function(){
    console.log(this.stacks[0])
    console.log(this.stacks[1])
    console.log(this.stacks[2])
  }

  Game.prototype.run = function(gameOver){
    var move;

    if ( gameOver == true ) {
      this.display();
      return;
    } else {
      this.display();
      move = this.getMove();
    }
  }

  Game.prototype.checkWin = function() {
    var found = false;

    for (var i = 1; i < this.stacks.length; i++) {
      if (this.stacks[i].join() == [3,2,1].join()) {
        found = true;
      }
    }

    if (found) {
      this.run(true);
    } else {
      this.run(false);
    }
  };

  Game.prototype.makeMove = function(move) {
    reader.close();

    var from, to, disc;
    from = move[0];
    to = move[1];
    disc = this.stacks[from].pop();
    this.stacks[to].push(disc);

    this.checkWin();
  };

  Game.prototype.getMove = function(){
    var game = this;
    var readline = require('readline');

    var parsedAnswer1, parsedAnswer2;

    reader = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });

    reader.question("From: ", function(answer1) {
      reader.question("To: ", function(answer2) {
        parsedAnswer1 = parseInt(answer1);
        parsedAnswer2 = parseInt(answer2);

        game.makeMove([parsedAnswer1, parsedAnswer2]);

      })
    });
  }

})(this);

g = new this.Hanoi.Game;
g.run(false);