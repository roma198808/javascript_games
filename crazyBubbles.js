var readline = require('readline');
var reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var askLessThan = function(el1, el2, callback){
  reader.question("Is " + el1 + " < " + el2 + "?", function(answer){
    if(answer == "yes"){
      callback(true);
    }else{
      callback(false);
    }
  })
}

var performSortPass = function(arr, i, madeAnySwaps, callback){
  if(i < arr.length - 1){
    askLessThan(arr[i], arr[i+1], function(lessThan){
      if(!lessThan){
        var temp = arr[i + 1];
        arr[i + 1] = arr[i];
        arr[i] = temp;
        performSortPass(arr, i+1, true, callback);
      }else{
        performSortPass(arr, i+1, false, callback);
      }
    })
  }

  if(i == (arr.length) - 1) {
    callback(madeAnySwaps);
  }
}


var crazyBubbleSort = function(arr, sortCompletionCallback) {
  var sortPassCallback = function(madeAnySwaps) {
    if(madeAnySwaps == true) {
      performSortPass(arr, 0, false, sortPassCallback);
    } else {
      reader.close();
      sortCompletionCallback(arr);
    }
  }

  sortPassCallback(true)
}

crazyBubbleSort([3, 2, 1], function (arr) { console.log(arr) });